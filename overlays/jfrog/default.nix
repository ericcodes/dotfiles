{ nixpkgs, flake-utils, ... }: system:
self: super:
let
  pkgs = nixpkgs.legacyPackages.${system};
  version = "1.42.3";
  src.x86_64-darwin = nixpkgs.legacyPackages.x86_64-darwin.fetchurl {
      url = "https://releases.jfrog.io/artifactory/jfrog-cli/v1/${version}/jfrog-cli-mac-386/jfrog";
      sha256 = "jjbSB/YoGjSFcXtzIiHO5a7WiHsdiQfeg/JE4FLGa90=";
  };
  src.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.fetchurl {
      url = "https://releases.jfrog.io/artifactory/jfrog-cli/v1/${version}/jfrog-cli-linux-amd64/jfrog";
      sha256 = "nMG+nj5tX6XSQAQM3Wujsr90qPzJ0HoZO5bq7fjeZKc=";
  };
  jfrog = src.${system};
in with pkgs;
  {
    jfrog = runCommand "jfrog" {  } ''
      mkdir -p $out/bin
      cp ${jfrog} $out/bin/jfrog
      chmod +x $out/bin/jfrog
    '';
  }
