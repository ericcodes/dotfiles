{ nixpkgs, ... }: system:
let
  pkgs = import nixpkgs { inherit system; };
  suds =
    { lib
    , python3
    , ...
    }:

    python3.pkgs.buildPythonPackage rec {
      pname = "suds";
      version = "1.1.2";

      src = python3.pkgs.fetchPypi {
        inherit pname version;
        extension = "tar.gz";
        sha256 = "sha256-HVz6dBFxk7JEpCM/JGxIPZ9BGYtEjF8UqLrRHE9knys=";
      };

      doCheck = false;

      meta = with lib; {
        description = "Lightweight SOAP client";
        license = licenses.lgpl3;
      };
    };
  chirp =
    super@{ lib
    , fetchFromGitHub
    , python3
    , unstableGitUpdater
    , ...
    }:

    python3.pkgs.buildPythonApplication rec {
      pname = "chirp";
      version = "unstable-2022-12-07";

      src = fetchFromGitHub {
        owner = "kk7ds";
        repo = "chirp";
        rev = "3f09fbda2064a2f45d735b2ec3ac07bf1157faab";
        hash = "sha256-tR6IXxu4CMlLVzk4yvc4xRYjrdF72TcE9t+4/hC+JD8=";
      };

      propagatedBuildInputs = with python3.pkgs; [
        future
        pyserial
        requests
        six
        wxPython_4_2
        yattag
        (suds super)
      ];

      # "running build_ext" fails with no output
      doCheck = false;

      passthru.updateScript = unstableGitUpdater {
        branch = "py3";
      };

      meta = with lib; {
        description = "A free, open-source tool for programming your amateur radio";
        homepage = "https://chirp.danplanet.com/";
        license = licenses.gpl3Plus;
        platforms = platforms.linux;
      };
    };
in
self: super:
{
  chirp-master = chirp super;
}
