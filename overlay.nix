{ nixpkgs, nixpkgs-unstable, protobuf-go, helmfile, emacsfromscratch, yq-go, ... }: system: self: super: {
  yq-go = yq-go.legacyPackages.${system}.yq-go;
  direwolf = nixpkgs-unstable.legacyPackages.${system}.direwolf;
  helmfile = helmfile.legacyPackages.${system}.helmfile;
  emacsfromscratch = emacsfromscratch.defaultPackage.${system};
  sdrangel = nixpkgs-unstable.legacyPackages.${system}.sdrangel;
  protoc-gen-go = nixpkgs.legacyPackages.${system}.buildGoModule rec {
    pname = "protoc-gen-go";
    version = "1.26.0";
    src = protobuf-go;
    vendorSha256 = "0rhgx3zkxp9gg4q7vck6x0ps5fp67lc0swbrgbpsghhribi2bgy9";
    subPackages = [ "cmd/protoc-gen-go" ];
  };
  yaml-language-server_1_14 = nixpkgs-unstable.legacyPackages.${system}.nodePackages.yaml-language-server;

  simplex-chat-desktop = nixpkgs-unstable.legacyPackages.${system}.simplex-chat-desktop;
}
