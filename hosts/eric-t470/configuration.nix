# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # ./modules/virtualbox.nix
      # ./cachix.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "eric-t470"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Enable Network Manager
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Enable the GNOME Desktop Environment.
  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    displayManager.gdm.wayland = true;
    desktopManager.gnome.enable = true;
  };

  services.gnome.gnome-remote-desktop.enable = true;

  # Enable opengl
  hardware.graphics.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # SANE for scanners
  hardware.sane.enable = true;

  security.rtkit.enable = true;

  # Enable touchpad support.
  services.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.groups.plugdev = { };
  users.users.eric = {
    uid = 1000;
    isNormalUser = true;
    extraGroups = [ "adbusers" "wheel" "networkmanager" "docker" "dialout" "plugdev" "tss" "scanner" "lp" "audio" ];
    shell = pkgs.zsh;
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  programs.steam.enable = true;

  environment.systemPackages = with pkgs; [
    zlib.dev
    zlib.out
    vim
    git
    keepassxc
    gnome.gnome-tweaks
    gsettings-desktop-schemas
    gnutls
    qemu

    usbutils

    # backups
    restic

    # TPM 2.0 utilities
    tpm2-tools

    # gnome extensions
    gnomeExtensions.appindicator
    gnomeExtensions.syncthing-indicator
    gnomeExtensions.weather-oclock

    google-chrome

    pavucontrol
    pwvucontrol
  ];

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # Enable TPM 2.0 support
  security.tpm2.enable = true;
  security.tpm2.pkcs11.enable = true;  # expose /run/current-system/sw/lib/libtpm2_pkcs11.so
  security.tpm2.tctiEnvironment.enable = true;  # TPM2TOOLS_TCTI and TPM2_PKCS11_TCTI env variables

  # List services that you want to enable:
  virtualisation.docker.enable = true;

  services.udev.packages = [ pkgs.libsigrok pkgs.edgetx pkgs.dfu-util ];

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    8002 # Direwolf KISS TNC
    # 3390 # Remote Login
    # 3389 # Desktop Sharing
    # 22   # ssh
  ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;


  # Enable Gnome's Virtual FS
  # services.gvfs.enable = true;

  # Important to resolve .local domains of printers, otherwise you get an error
  # like  "Impossible to connect to XXX.local: Name or service not known"
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    publish = {
      enable = true;
      addresses = true;
      domain = true;
      hinfo = true;
      userServices = true;
      workstation = true;
    };
  };

  programs.zsh.enable = true;

  programs.adb.enable = true;

  nix = {
    settings.substituters = [
      "https://cache.nixos.org"
      "https://cache.dhall-lang.org"
      "https://dhall.cachix.org"
    ];

    settings.trusted-public-keys = [
      "cache.dhall-lang.org:I9/H18WHd60olG5GsIjolp7CtepSgJmM2CsO813VTmM="
      "dhall.cachix.org-1:8laGciue2JBwD49ICFtg+cIF8ddDaW7OFBjDb/dHEAo="
      "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
    ];

    settings.trusted-users = [
      "root"
      "@wheel"
    ];
  };

  networking.extraHosts = ''
    192.168.1.3 k8s-master.local
  '';

  nix.settings.auto-optimise-store = true;
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = [ "eric" ];
  hardware.rtl-sdr.enable = true;

  # Enable the ability to emulate ARM and Raspberry PI binaries
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  # backups
  services.restic.backups.eric-home = {
    paths = [
      "/home/eric"
    ];

    extraBackupArgs = [
      "--verbose"
      "--exclude-caches"
      "--exclude=/home/eric/.cache"
      "--exclude=/home/eric/.cargo"
    ];

    timerConfig = {
      OnCalendar = "daily";
      Persistent = true;
    };

    # This requires the the public key of root is copied to the server's authorized keys
    repository = "sftp://u387620-sub3@u387620-sub3.your-storagebox.de:23//home/restic-repo";


    # Put the repo's password in this file. chown root:root, chmod 600
    passwordFile = "/etc/restic/eric-home.pass";
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?

  services.pipewire.alsa.enable = true;
}
