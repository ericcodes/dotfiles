# dotfiles

These are my dotfiles and home-manager configuration

## Requirements

- NixOS or [nix-darwin](https://github.com/LnL7/nix-darwin)

## Add binary cache

[Follow the instructions to configure the nix-community binary cache](https://app.cachix.org/cache/nix-community)

## Activate

``` sh
./scripts/hm-switch
```

## TODO

- [x] Setup eric-t470 for home-manager + flakes
- [x] Setup work computer for home-manager + flakes
- [x] move NixOS configuration into the flake
- [ ] Figure out how to get Emacs in spotlight

The branch was renamed to main, follow these [instructions](https://docs.github.com/en/repositories/configuring-branches-and-merges-in-your-repository/managing-branches-in-your-repository/renaming-a-branch#updating-a-local-clone-after-a-branch-name-changes) on how to update the local clones
