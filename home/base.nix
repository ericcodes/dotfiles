privates: pkgs: {
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  home.packages = with pkgs; [
    aws-iam-authenticator
    jfrog

    # fonts
    nanum-gothic-coding
    # symbola # emoji font for doom-emacs
    comic-relief
    julia-mono
    jost
    recursive
    iosevka-bin
    google-fonts
    intel-one-mono
    ultimate-oldschool-pc-font-pack

    # fancy CLI tools
    ripgrep
    sd
    bat
    fd
    gum

    # tools
    gitAndTools.hub
    git-lfs
    postgresql
    findutils
    dig
    obsidian

    # metadata stripper
    mat2

    # Tools needed by doom emacs's modules
    (aspellWithDicts (dicts: with dicts; [ en en-computers en-science ]))
    nixfmt-classic
    shellcheck
    proselint
    pandoc
    mdl
    gnutls
    gnupg
    pass
    graphviz
    jsonnet
    kubectl
    kubernetes-helm
    kubectx
    helmfile
    jq
    languagetool

    # misc
    xdg-utils
    sqlite
    unixtools.watch
    wrk

    # stack for Haskell scripts
    stack

    yq-go

    emacsfromscratch

    # nix language server
    nil

    yaml-language-server_1_14
    # nodePackages.vscode-langservers-extracted
    nodePackages.dockerfile-language-server-nodejs
    nodePackages.bash-language-server
    nodePackages.typescript-language-server

    # global guile install so that I can use it in Emacs outside of a guile project
    guile_3_0

    sbcl
    clisp

    python311
    zsh-powerlevel10k
  ];

  programs = {
    git = {
      extraConfig = {
        url = { "git@github.com:" = { insteadOf = "https://github.com/"; }; };
        pull = { rebase = true; };
      };
      includes = [
        {
          condition = "hasconfig:remote.*.url:git@gitlab.com:ericcodes/**";
          contents = {
            user = {
              name = privates.personas.home.name;
              email = privates.personas.home.email;
            };
          };
        }

        {
          condition = "hasconfig:remote.*.url:git@github.com:corshatech/**";
          contents = {
            user = {
              name = privates.personas.work.name;
              email = privates.personas.work.email;
            };
          };
        }
      ];
    };
  };

  programs.zsh = {
    enable = true;
    enableCompletion = true;

    shellAliases = {
      k8s_secrets = "jq '.data | with_entries(.value |= (. | @base64d))'";
    };
    initExtra = ''
if [ -e $HOME/.p10k.zsh ]; then
  source $HOME/.p10k.zsh
fi
source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme
'';
  };

  home.sessionVariables = {
    CLICOLOR = 1;
    EDITOR = "emacsclient";
    EMACS_SOCKET_NAME = "$HOME/src/emacsfromscratch/.emacs.d/var/server/server";
  };

  home.file = {
    ".cargo/config.toml".text = ''
      [net]
      git-fetch-with-cli = true
    '';
    ".guile".text = ''
;; from https://gist.github.com/mhitza/a00d7900571e9f13bac2bbf4a203d21b
(use-modules (ice-9 readline))
(activate-readline)
'';
  };

  fonts.fontconfig.enable = true;

  programs.direnv.enable = true;
  programs.direnv.nix-direnv.enable = true;

}
