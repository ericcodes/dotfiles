pkgs: {
  home.packages = with pkgs; [
    # utilities
    appimage-run
    file
    gcc
    gnugrep
    keepassxc
    pinentry-curses
    sops
    yq-go
    libsForQt5.kdeconnect-kde
    libsForQt5.skanpage

    # wine support
    bottles
    wineWowPackages.stable

    # comms
    discord
    signal-desktop
    slack
    zoom-us
    simplex-chat-desktop
    element-desktop
    protonvpn-gui
    # thunderbird

    # entertainment
    # minecraft
    # steam
    # steam-run

    # content consumption
    evince
    ungoogled-chromium
    vlc

    # content creation
    imagemagick
    inkscape
    krita
    obs-studio
    openscad
    bambu-studio

    # R&D
    calibre
    zotero
    anki

    # gnome themes
    arc-icon-theme
    arc-theme

    audacity

    # printer driver?
    mfcj6510dw-cupswrapper
    mfcj6510dwlpr

    # radio
    cubicsdr
    dsd
    dsdcc
    gqrx
    sdrangel
    sdrpp
    socat

    arduino-ide
  ];

  xdg.mimeApps = {
    enable = true;
    defaultApplications = {
      "application/pdf" = ["org.gnome.Evince.desktop"];
      "application/x-extension-ics" = [ "userapp-Thunderbird-WAZEH2.desktop" ];
      "message/rfc822" = [ "userapp-Thunderbird-KZHZH2.desktop" ];
      "text/calendar" = [ "userapp-Thunderbird-WAZEH2.desktop" ];
      "x-scheme-handler/mailto" = [ "userapp-Thunderbird-KZHZH2.desktop" ];
      "x-scheme-handler/mid" = [ "userapp-Thunderbird-KZHZH2.desktop" ];
      "x-scheme-handler/msteams" = [ "teams.desktop" ];
      "x-scheme-handler/unknown" = [ "firefox.desktop" ];
      "x-scheme-handler/webcal" = [ "userapp-Thunderbird-WAZEH2.desktop" ];
      "x-scheme-handler/webcals" = [ "userapp-Thunderbird-WAZEH2.desktop" ];
    };
  };

  home.sessionVariables = {
    NIX_PATH = "$HOME/.nix-defexpr/channels\${NIX_PATH:+:}$NIX_PATH";
  };

  fonts.fontconfig.enable = true;
  services.syncthing.enable = true;

  services.nextcloud-client = {
    enable = true;
    startInBackground = true;
  };
}
