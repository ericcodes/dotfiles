pkgs: {
  home.packages = with pkgs; [
    terminal-notifier
    azure-cli
    terraform
    awscli
    plantuml


    # rust tooling
    rustup

    # nodejs tooling
    nodejs_22
  ];

  home.sessionVariables = {
    GOPRIVATE = "github.com/corshatech/*";
    NIX_PATH = "$HOME/.nix-defexpr/channels\${NIX_PATH:+:}$NIX_PATH";
    NVM_DIR = "$HOME/.nvm";
  };

  programs.zsh = {
    shellAliases = {
      flushdns =
        "sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder";
    };

    initExtra = ''
      # Installed via `brew install gnu-getopt` this is required so the brew's
      # gnu-getopt overrides the system getopt
      export PATH="/usr/local/opt/gnu-getopt/bin:$PATH"
      export PATH="$HOME/.krew/bin:$PATH"

      # The location of the go install binaries
      export PATH="$HOME/go/bin:$PATH"

      # required to pick up cargo installed binaries
      export PATH="$HOME/.cargo/bin:$PATH"

      # required for the different k8s env
      export KUBECONFIG=$KUBECONFIG:~/.kube/config:~/.kube/prod-config:~/.kube/staging-config

      # required by nvm which was installed using nvm's script
      function activate_nvm {
        [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
        [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
      }

      # required by `brew install --cask google-cloud-sdk`
      source "$(brew --prefix)/share/google-cloud-sdk/path.zsh.inc"
      source "$(brew --prefix)/share/google-cloud-sdk/completion.zsh.inc"
    '';
  };
}
