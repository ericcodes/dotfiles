{
  description = "Home Manager Config";

  nixConfig = {
    extra-substituters = [
      "https://cachix.cachix.org"
      "https://nix-community.cachix.org"
    ];

    extra-trusted-public-keys = [
      "cachix.cachix.org-1:eWNHQldwUO7G2VkjpnjDbWwy4KQ/HNxht7H4SSoMckM="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    nixpkgs = { url = "github:nixos/nixpkgs/nixos-24.11"; };
    nixpkgs-unstable = { url = "github:nixos/nixpkgs/nixos-unstable"; };

    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils = {
      url = "github:numtide/flake-utils";
    };

    # Corsha Docker build image versions
    helmfile.url = "github:nixos/nixpkgs/5b455776e301b85e1180f71b9237b37e00f7134f"; # 0.141.0
    yq-go.url = "github:nixos/nixpkgs/a9a110ab58124e406d0739784da5b922feb0a2f8"; # 4.27.2

    protobuf-go = {
      url = "github:protocolbuffers/protobuf-go/v1.26.0";
      flake = false;
    };

    nixos-hardware.url = "github:nixos/nixos-hardware";

    emacsfromscratch = {
      url = "git+ssh://git@gitlab.com/ericcodes/emacsfromscratch.git?ref=main";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    ham-radio = {
      url = "gitlab:ericcodes/ham-radio";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    systems.url = "github:nix-systems/x86_64-linux";

    # I need to figure out how to encrypt these, but the values are not
    # secrets so a private repo is fine for now.
    privates.url = "git+ssh://git@gitlab.com/ericcodes/dotfiles-privates?ref=main";
  };

  outputs =
    inputs@{ self, nixpkgs, home-manager, nixos-hardware, privates, ham-radio, ... }:
    let
      t470 = privates.personas.home.hostname;
      # I need to anonymize this PII before committing it
      macbook = privates.personas.work.hostname;
      overlaysFun = system: [
        (import ./overlay.nix inputs system)
        (import ./overlays/jfrog inputs system)
        (import ./overlays/chirp.nix inputs system)
      ];
    in
    {
      nixosConfigurations.eric-t470 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          nixos-hardware.nixosModules.lenovo-thinkpad-t470s
          nixos-hardware.nixosModules.common-pc-laptop-ssd
          ham-radio.nixosModules.ft-991a
          ({ ... }: { hardware.ft-991a.enable = true; })
          ./hosts/eric-t470/configuration.nix
        ];
      };
      homeConfigurations = {
        "${t470}" =
          let
            system = "x86_64-linux";
            overlays = overlaysFun system;
            pkgs = import nixpkgs {
              inherit system overlays;
              config.allowUnfree = true;

              config.permittedInsecurePackages = pkgs.lib.optional (pkgs.obsidian.version == "1.4.16") "electron-25.9.0";
            };
          in
          home-manager.lib.homeManagerConfiguration {
            inherit pkgs;
            modules = [
              {
                home = {
                  stateVersion = "21.05";
                  username = "eric";
                  homeDirectory = "/home/eric";
                };

                services.gpg-agent = {
                  enable = true;
                  extraConfig = ''
allow-emacs-pinentry
allow-loopback-pinentry
'';
                };

                programs.git = {
                  enable = true;
                  userName = privates.personas.home.name;
                  userEmail = privates.personas.home.email;
                  signing = {
                    key = privates.personas.home.email;
                    signByDefault = true;
                  };
                  extraConfig.init.defaultBranch = "main";
                };
              }
              (import ./home/base.nix privates pkgs)
              (import ./home/platforms/nixos.nix pkgs)
            ];
          };

        "${macbook}" =
          let
            system = "x86_64-darwin";
            overlays = overlaysFun system;
            pkgs = import nixpkgs {
              inherit system overlays;
              config.allowUnfree = true;
              config.permittedInsecurePackages = pkgs.lib.optional (pkgs.obsidian.version == "1.4.16") "electron-25.9.0";
            };
          in
          home-manager.lib.homeManagerConfiguration {
            inherit pkgs;
            modules = [
              {
                home = {
                  stateVersion = "21.05";
                  username = "eric";
                  homeDirectory = "/Users/eric";
                };

                programs.git = {
                  enable = true;
                  userName = privates.personas.work.name;
                  userEmail = privates.personas.work.email;
                  signing = {
                    key = privates.personas.work.email;
                    signByDefault = true;
                  };
                };
              }
              (import ./home/base.nix privates pkgs)
              (import ./home/platforms/darwin.nix pkgs)
            ];
          };
      };

      packages."x86_64-linux" = {
        "${t470}" = self.homeConfigurations."${t470}".activationPackage;
      };
      packages."x86_64-darwin" = {
        "${macbook}" = self.homeConfigurations."${macbook}".activationPackage;
      };
    };
}
